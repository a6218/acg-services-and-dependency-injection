import { EventEmitter, Injectable } from '@angular/core';
import { Account } from 'src/app/types/account/account';
import { LogAccountService } from '../log-account-service/log-account.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private logAccountService: LogAccountService) { }

  accounts = [
    {
      name: 'Master Account',
      status: 'active'
    },
    {
      name: 'Testaccount',
      status: 'inactive'
    },
    {
      name: 'Hidden Account',
      status: 'unknown'
    }
  ];

  statusUpdated = new EventEmitter<string>();

  addAccount(name: string, status: string) {
    const newAccount: Account = {
      name: name,
      status: status,
    };
    this.accounts.push(newAccount);
    this.logAccountService.logAccountStatus(status);
  }

  updateStatus(id: number, newStatus: string) {
    this.accounts[id].status = newStatus;
    this.logAccountService.logAccountStatus(newStatus);
  }
}
