import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LogAccountService {
  constructor() { }

  logAccountStatus(accountStatus: string) {
    console.log('A server status changed, new status: ' + accountStatus);
  }
}
