import { TestBed } from '@angular/core/testing';

import { LogAccountService } from './log-service.service';

describe('LogServiceService', () => {
  let service: LogAccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LogAccountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
