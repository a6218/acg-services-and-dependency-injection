import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CounterService {
  activeCounter: number = 0;
  inactiveCounter: number = 0;

  activeCounterStatus = new EventEmitter<number>();
  inactiveCounterStatus = new EventEmitter<number>();

  setActive() {
    this.activeCounter++;
    this.activeCounterStatus.emit(this.activeCounter);
  }

  setInactive() {
    this.inactiveCounter++;
    this.inactiveCounterStatus.emit(this.inactiveCounter);
  }
}
