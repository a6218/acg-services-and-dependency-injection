import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccountComponent } from './components/account/account/account.component';
import { NewAccountComponent } from './components/new-account/new-account/new-account.component';
import { TaskComponent } from './components/task/task/task.component';
import { ActiveUsersComponent } from './components/task/task/active-users/active-users/active-users.component';
import { InactiveUsersComponent } from './components/task/task/inactive-users/inactive-users/inactive-users.component';

@NgModule({
  declarations: [
    AppComponent,
    AccountComponent,
    NewAccountComponent,
    TaskComponent,
    ActiveUsersComponent,
    InactiveUsersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
