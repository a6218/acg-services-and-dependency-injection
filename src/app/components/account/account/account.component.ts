import { Component, Input } from '@angular/core';
import { AccountService } from 'src/app/services/account-service/account.service';
import { Account } from 'src/app/types/account/account';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
})
export class AccountComponent {
  @Input() account!: Account;
  @Input() id!: number;

  constructor(private accoutService: AccountService) { }

  onSetTo(status: string) {
    this.accoutService.updateStatus(this.id, status);
    this.accoutService.statusUpdated.emit(status);
  }
}
