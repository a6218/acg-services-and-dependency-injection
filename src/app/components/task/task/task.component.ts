import { Component, OnInit } from '@angular/core';
import { CounterService } from 'src/app/services/counter-service/counter.service';
import { UsersService } from 'src/app/services/users-service/users.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  activeUsers: string[] = [];
  inactiveUsers: string[] = [];
  activeCounter: number = 0;
  inactiveCounter: number = 0;

  constructor(
    private usersService: UsersService,
    private counterService: CounterService
  ) { }

  ngOnInit(): void {
    this.activeUsers = this.usersService.activeUsers;
    this.inactiveUsers = this.usersService.inactiveUsers;

    this.counterService.activeCounterStatus.subscribe(
      (activeCounter: number) => {
        this.activeCounter = activeCounter;
      }
    );

    this.counterService.inactiveCounterStatus.subscribe(
      (inactiveCounter: number) => this.inactiveCounter = inactiveCounter,
    );
  }
}
