import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/account-service/account.service';
import { LogAccountService } from 'src/app/services/log-account-service/log-account.service';

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.scss'],
})
export class NewAccountComponent implements OnInit {

  constructor(private accountService: AccountService) {}

  ngOnInit(): void {
    this.accountService.statusUpdated.subscribe(
      (status: string) => {
        console.log('status: ', status);
      },
    );
  }

  onCreateAccount(name: string, status: string) {
    this.accountService.addAccount(name, status);
  }
}
